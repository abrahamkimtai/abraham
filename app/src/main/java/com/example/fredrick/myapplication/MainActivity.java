package com.example.fredrick.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void screenOne(View view){
        Intent intent=new Intent(this,Activity1.class);
        //intent.putExtra(EXTRA_MESSAGE,message);
        startActivity(intent);


    }
    public void screenTwo(View view){
        Intent intent=new Intent(this,ScreenTwo.class);
        startActivity(intent);
    }
    public void screenThree(View view){
        Intent intent=new Intent(this,ScreenThree.class);
        startActivity(intent);
    }
}
